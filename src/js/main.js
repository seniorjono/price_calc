function checkform() {

	var results = document.getElementById("results");

	// Select all elements from within the form
	var f = document.forms["priceCalc"].elements;

	// Initially set trigger to true
	var cansubmit = true;

	// For every object within the form, work out if the length is larger than 0
	for (var i = 0; i < f.length; i++) {
		if (f[i].value.length == 0) cansubmit = false;
	}

	if (cansubmit) {
		results.classList.remove("form__container__result--hidden");
	} else {
		results.classList.add("form__container__result--hidden");
	}
}








function calculatePrice(priceCalc) {

	// Set these as defaults
	var currency = 1;
	var currencySign = "£";
	var tierDecSub = 0.03;

	//Set all variables from page data  
	var elt = document.getElementById("usersItem");
	var users = elt.value;


	// If users less than 5 then instantly change to 5
	if (users < 5) {
		elt.value = 5;
	}
	// If users moren than 250 then instantly change to 250
	if (users > 250) {
		elt.value = 250;
	}

	var elt = document.getElementById("tierItem");
	var tier = elt.options[elt.selectedIndex].value;

	var elt = document.getElementById("currencyItem");
	var currency = elt.options[elt.selectedIndex].value;


	// Tier list for deciding decimal subtraction
	if (tier == 25) {
		tierDecSub = 0.03;
	}
	if (tier == 45) {
		tierDecSub = 0.055;
	}
	if (tier == 85) {
		tierDecSub = 0.155;
	}

	// Tier list for deciding decimal subtraction
	if (currency == 1) {
		currencySign = "£";
	}
	if (currency == 1.35) {
		currencySign = "$";
	}
	if (currency == 1.1) {
		currencySign = "€";
	}


	// Purely for debugging purposes
	console.log("-----------------------------\n\nUsers: " + users + "\nTier: " + tier + "\nPricing Tier Subtraction: " + tierDecSub + "\nCurrency : " + currency);

	// Do that calculation that tim made that I have no idea what it does
	var initialCalc = tier * users - tierDecSub * Math.pow((users - 5), 2);

	//calculate total value for the first part
	var singleUser = (initialCalc * currency) / users;

	var monthlyTotal = singleUser * users;

	//print value to bottom results table, prefix results with currency sign from dropdown
	document.getElementById("TotalPrice").value = currencySign + (singleUser).toFixed(2);
	document.getElementById("MonthlyPrice").value = currencySign + (monthlyTotal).toFixed(2);


}
